import Config from "../src/index.config";
import Server from "../src/server/Server";
import * as reqwest from 'request';
import { getConnection } from "typeorm";
import UserTask from "../src/entity/UserTask";
import UserTaskController from "../src/server/route/UserTaskController";
import Task from "../src/entity/Task";
import { UserRepository } from "../src/entity/UserRepository";
import User from "../src/entity/User";
// import * as express from 'express';


describe('UserTaskController', () => {

    test('Create Route gets correctly configured', async () => {
        return new Promise(async (resolve: any, reject: any) => {
    
            // routesConfig.routes[0].callbackClass
            const route = jest.spyOn(UserTaskController.prototype, 'create');
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/usertask/create',
                    method: 'POST',
                    json: {userId: 1, taskId: "1", completed: "1521983083"},
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toBe(200);
                    expect(body.result.userTaskId).toBeDefined();
                    expect(body.error).toBe(false);
                    expect(route).toHaveBeenCalled();
                    expect(route).toHaveBeenCalledTimes(1);
                    //remove test entry from db, to keep it clean
                    await getConnection()
                        .createQueryBuilder()
                        .delete()
                        .from(UserTask)
                        .where("id = :id", { id: body.result.userTaskId })
                        .execute();
                    resolve();
                }
            );
        });
    });


    test('Create Route rejects incorrect parameters', async () => {
        return new Promise(async (resolve: any, reject: any) => {
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/usertask/create',
                    method: 'POST',
                    json: {userId: 1, taskId: "1"}, // completed: "1521983083" missing
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toBe(400);
                    expect(body.error).toBe(true);
                    expect(body.message).toBe('invalid parameters send');
                    resolve();
                }
            );
        });
    });


    test('Create Route rejects userId which is not valid', async () => {
        return new Promise(async (resolve: any, reject: any) => {
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/usertask/create',
                    method: 'POST',
                    json: {userId: 0, taskId: "1", completed: "1521983083"},
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toBe(400);
                    expect(body.error).toBe(true);
                    expect(body.message).toBe('unmatched parameters send');
                    resolve();
                }
            );
        });
    });







    test('Done Route gets correctly configured', async () => {
        return new Promise(async (resolve: any, reject: any) => {
            // routesConfig.routes[0].callbackClass
            const route = jest.spyOn(UserTaskController.prototype, 'done');
            const server = new Server(Config);
            
            await server.start();

            const user = new User();
            user.firstName = 'JestTest';
            user.lastName = 'TestJest';
            user.password = 'JestTesting';
            const newUser = await getConnection().manager.save(user);

            const task = new Task();
            task.description = 'a';
            task.name = 'b';
            task.value = 2;
            const newTask1 = await getConnection().manager.save(task);


            const userTask1 = new UserTask();
            userTask1.user = user;
            userTask1.completed = new Date(Date.now());
            userTask1.task = newTask1;

            const userTask2 = new UserTask();
            userTask2.user = user;
            userTask2.completed = new Date(Date.now());
            userTask2.task = newTask1;

            const newUserTask1 = await getConnection().manager.save(userTask1);
            const newUserTask2 = await getConnection().manager.save(userTask2);

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/usertask/done?userId=' + newUser.id,
                    method: 'GET',
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    body = JSON.parse(body);

                    await server.stop();

                    await getConnection().manager.remove(newUserTask1);
                    await getConnection().manager.remove(newUserTask2);
                    await getConnection().manager.remove(newTask1);
                    await getConnection().manager.remove(newUser);

                    // console.log(body);
                    expect(response.statusCode).toBe(200);
                    expect(body.result.totalPoints).toBe(4);
                    expect(body.error).toBe(false);
                    expect(route).toHaveBeenCalled();
                    expect(route).toHaveBeenCalledTimes(1);
                    
                    resolve();
                }
            );
        });
    });


    test('Done Route rejects incorrect parameters', async () => {
        return new Promise(async (resolve: any, reject: any) => {
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/usertask/done',
                    method: 'GET',
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    body = JSON.parse(body);
                    await server.stop();

                    // console.log(body);
                    expect(response.statusCode).toEqual(400);
                    expect(body.error).toEqual(true);
                    expect(body.message).toEqual('invalid parameters send');
                    resolve();
                }
            );
        });
    });


});