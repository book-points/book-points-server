import User from "../src/entity/User";
import { BaseEntity } from "typeorm";

describe('Entity.User', () => {

    test('User inherits BaseEntity', () => {
        const user = new User();
        
        expect(user).toBeInstanceOf(BaseEntity);
    });

    test('User.firstName', () => {
        const user = new User();
        user.firstName = 'Niklas';

        expect(user.firstName).toEqual('Niklas');
    });

    test('User.lastName', () => {
        const user = new User();
        user.lastName = 'Maier';

        expect(user.lastName).toEqual('Maier');
    });

    test('User.password', () => {
        const user = new User();
        user.password = '2';

        expect(user.password).toEqual('2');
    });

});