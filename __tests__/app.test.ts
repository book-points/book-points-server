import DBO from './../src/dbo/DBO';
import * as config from './../src/index.config';
import * as express from 'express';
import {Request, Response} from 'express';
import { Connection, getConnection } from 'typeorm';
import User from './../src/entity/User';
import UserController from './../src/server/route/UserController';
import Server from './../src/server/Server';
import Config from './../src/index.config';
import BaseEntity from '../src/entity/Base';
import RestController from '../src/server/route/RestController';

let port = 1338;

describe('App / index: ', () => {

    test('Initialises', () => {
        //imports inside of functions dont work
        const index = require('./../src/index');
        
        return new Promise((resolve, reject) => {
            index.default.globalSignals.ready.add(() => {
                
                expect(index.default.app.server).toBeInstanceOf(Server);

                index.default.app.server.stop().then(() => resolve());
            });
        });
    });
});


describe('DBO: ', () => {

    test('DBO establishes connection', async () => {
    
        await expect(DBO.connection(Config.dboConfig)).resolves.toBeInstanceOf(Connection);
        expect(getConnection().isConnected).toEqual(true);
        expect(DBO.Connection).toBeInstanceOf(Connection);
        expect(DBO.connection(Config.dboConfig)).resolves.toBeInstanceOf(Connection);
        await DBO.close();
    });
    
    test('DBO fails establishing connection', () => {
    
        const falseConfig = {
            name: 'default_falsy_second',
            type: 'mysql',
            host: '127.0.0.1',
            port: 3306,
            username: 'root',
            password: 'rootLorem',
            database: 'BookPoints',
            entities: [
                User
            ],
            synchronize: true,
            logging: false
        };
        
        return DBO.connection(falseConfig).catch(e => {
            const con = getConnection();
            expect(con.isConnected).toEqual(false);
        });
    }); // , 11000
});