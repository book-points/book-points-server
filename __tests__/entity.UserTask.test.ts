import UserTask from "../src/entity/UserTask";
import { BaseEntity } from "typeorm";

describe('Entity.UserTask', () => {
    
    test('UserTask inherits BaseEntity', () => {
        const task = new UserTask();
        
        expect(task).toBeInstanceOf(BaseEntity);
    });
});