import Config from "../src/index.config";
import Server from "../src/server/Server";
import * as reqwest from 'request';
import { getConnection } from "typeorm";
import Task from "../src/entity/Task";
// import * as express from 'express';


describe('TaskController', () => {

    let taskId;
    test('Create Route gets correctly configured', async () => {
        return new Promise(async (resolve: any, reject: any) => {
    
            const route = jest.spyOn(Config.routesConfig.namespaces[1].callbackClass.prototype, 'create');
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/task/create',
                    method: 'POST',
                    json: {name: "jest-test", description: "testing-jest", value: "2"},
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toEqual(200);
                    expect(body.error).toEqual(false);
                    expect(body.result.taskId).toBeDefined();
                    expect(route).toHaveBeenCalled();
                    expect(route).toHaveBeenCalledTimes(1);

                    //to remove test entry in upfollowing test from db, to keep it clean
                    taskId = body.result.taskId;

                    resolve();
                }
            );
        });
    });


    test('All Route returns all tasks', async () => {
        return new Promise(async (resolve: any, reject: any) => {
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/task/all',
                    method: 'GET',
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    body = JSON.parse(body);
                    await server.stop();
                    expect(response.statusCode).toBe(200);
                    expect(body.error).toBe(false);
                    expect(body.result.length).toBeGreaterThan(0);
                    resolve();
                }
            );
        });
    });


    test('Create Route rejects task whichs name is already taken', async () => {
        return new Promise(async (resolve: any, reject: any) => {
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/task/create',
                    method: 'POST',
                    json: {name: "jest-test", description: "testing-jest", value: "2"},
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toEqual(400);
                    expect(body.error).toEqual(true);
                    expect(body.message).toEqual('task.name already taken');
                    //remove test entry from db, to keep it clean
                    await getConnection()
                        .createQueryBuilder()
                        .delete()
                        .from(Task)
                        .where("id = :id", { id: taskId })
                        .execute();
                    resolve();
                }
            );
        });
    });


    test('Create Route rejects incorrect parameters', async () => {
        return new Promise(async (resolve: any, reject: any) => {
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/task/create',
                    method: 'POST',
                    json: {name: "jest-test", description: "testing-jest"}, // value: "2" missing
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toEqual(400);
                    expect(body.error).toEqual(true);
                    expect(body.message).toEqual('invalid parameters send');
                    resolve();
                }
            );
        });
    });

});