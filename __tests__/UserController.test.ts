import Server from './../src/server/Server';
import Config from './../src/index.config';
import * as reqwest from 'request';
import * as express from 'express';
import UserController from '../src/server/route/UserController';
import { getConnection, getCustomRepository } from 'typeorm';
import User from '../src/entity/User';
import UserTask from '../src/entity/UserTask';
import Task from '../src/entity/Task';
import { UserRepository } from '../src/entity/UserRepository';

let port = 1338;

describe('UserController', () => {

    let testUserId;

    test('Create Route gets correctly configured', async () => {
        return new Promise(async (resolve: any, reject: any) => {
    
            // routesConfig.routes[0].callbackClass
            const route = jest.spyOn(UserController.prototype, 'create');
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/user/create',
                    method: 'POST',
                    json: {
                        firstName: 'JestTest',
                        lastName: 'TestJest',
                        password: 'JestTesting'
                    },
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toBe(200);
                    expect(body.error).toBe(false);
                    expect(body.result.id).toBeDefined();
                    testUserId = body.result.id;
                    expect(route).toHaveBeenCalled();
                    expect(route).toHaveBeenCalledTimes(1);
                    resolve();
                }
            );
        });
    });


    test('All route returns user + total points', async () => {
        return new Promise(async (resolve: any, reject: any) => {
            
            const route = jest.spyOn(UserController.prototype, 'all');
        
            const server = new Server(Config);
            
            await server.start();

            const task = new Task();
            task.description = 'a';
            task.name = 'b';
            task.value = 2;
            const newTask1 = await getConnection().manager.save(task);

            const user = await getCustomRepository(UserRepository).findOneById(testUserId);

            const userTask1 = new UserTask();
            userTask1.user = user;
            userTask1.completed = new Date(Date.now());
            userTask1.task = newTask1;

            const userTask2 = new UserTask();
            userTask2.user = user;
            userTask2.completed = new Date(Date.now());
            userTask2.task = newTask1;
            
            const newUserTask1 = await getConnection().manager.save(userTask1);
            const newUserTask2 = await getConnection().manager.save(userTask2);

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/user/all',
                    method: 'GET',
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    body = JSON.parse(body);

                    await getConnection().manager.remove(newUserTask1);
                    await getConnection().manager.remove(newUserTask2);
                    await getConnection().manager.remove(newTask1);
                    await server.stop();
                    
                    // console.log(body);
                    expect(response.statusCode).toBe(200);
                    expect(body.error).toBe(false);
                    expect(body.result).toBeInstanceOf(Array);
                    expect(route).toHaveBeenCalled();

                    body.result.map(res => {
                        if(res.firstName === 'JestTest') {
                            expect(res.totalPoints).toBe(4);
                        }
                    });
                    resolve();
                }
            );
        });
    });


    test('Create route denies doubled firstName(username), if already taken', async () => {
        return new Promise(async (resolve: any, reject: any) => {
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/user/create',
                    method: 'POST',
                    json: {
                        firstName: 'JestTest',
                        lastName: 'TestJest',
                        password: 'JestTesting'
                    },
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    //remove test user from db, to keep it clean and make it work next test run again
                    await getConnection()
                        .createQueryBuilder()
                        .delete()
                        .from(User)
                        .where("firstName = :firstName", { firstName: 'JestTest' })
                        .execute();
                    expect(response.statusCode).toEqual(400);
                    expect(body.error).toEqual(true);
                    expect(body.message.length).toBeGreaterThan(5);
                    resolve();
                }
            );
        });
    });


    test('Create route denies invalid parameters', async () => {
        return new Promise(async (resolve: any, reject: any) => {
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/user/create',
                    method: 'POST',
                    json: {
                        firstName: '',
                        lastName: 'notEmpty'
                        // password: ''
                    },
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toEqual(400);
                    expect(body.error).toEqual(true);
                    expect(body.message.length).toBeGreaterThan(5);
                    resolve();
                }
            );
        });
    });


    test('Auth route successfully logs in user', async () => {
        return new Promise(async (resolve: any, reject: any) => {
    
            // routesConfig.routes[0].callbackClass
            // const route = jest.spyOn(Config.routesConfig.namespaces[0].callbackClass.prototype, 'auth');
            const route = jest.spyOn(UserController.prototype, 'auth');
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/user/auth',
                    method: 'POST',
                    json: {
                        firstName: 'niklas',
                        password: 'maier'
                    },
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toEqual(200);
                    expect(body.error).toEqual(false);
                    expect(body.message.length).toBeGreaterThan(5);
                    expect(body.result.id).toBeDefined();
                    expect(route).toHaveBeenCalled();
                    expect(route).toHaveBeenCalledTimes(1);
                    resolve();
                }
            );
        });
    });


    test('Auth route denies user login with wrong password', async () => {
        return new Promise(async (resolve: any, reject: any) => {

            const route = jest.spyOn(UserController.prototype, 'auth');
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/user/auth',
                    method: 'POST',
                    json: {
                        firstName: 'niklas',
                        password: 'loremFalse'
                    },
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toEqual(400);
                    expect(body.error).toEqual(true);
                    expect(body.message.length).toBeGreaterThan(5);
                    expect(body.result.id).not.toBeDefined();
                    expect(route).toHaveBeenCalled();
                    resolve();
                }
            );
        });
    });


    test('Auth route denies user login with wrong parameters', async () => {
        return new Promise(async (resolve: any, reject: any) => {
            
            const route = jest.spyOn(UserController.prototype, 'auth');
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/user/auth',
                    method: 'POST',
                    json: {
                        firstName: 'niklas',
                        // password: 'loremFalse'
                    },
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toEqual(400);
                    expect(body.error).toEqual(true);
                    expect(body.message.length).toBeGreaterThan(5);
                    expect(body.result.id).not.toBeDefined();
                    expect(route).toHaveBeenCalled();
                    resolve();
                }
            );
        });
    });

    
});