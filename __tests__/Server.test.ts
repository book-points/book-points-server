import Server from './../src/server/Server';
import Config from './../src/index.config';
import * as reqwest from 'request';
import * as express from 'express';


describe('Server', () => {
    // let server;
    
    // beforeEach(() => {
    //     server = new Server(Config);
    // });
    // afterEach(() => {
    //     return new Promise((resolve, reject) => {
    //         server.stop().then(() => resolve());
    //     });
    // });

    test('Server starts', async () => {
        return new Promise(async (resolve: any, reject: any) => {
            const server = new Server(Config);
        
            server.start().then(async status => {
                await server.stop();
                expect(status).toEqual(true);
                resolve();
            });

        });
    }); // , 11000


    test('Server denies with http 403 without correct authorization header sent', async () => {
        return new Promise(async (resolve: any, reject: any) => {
        
            const server = new Server(Config);
            
            await server.start();

            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/usertask/create',
                    method: 'POST',
                    json: {userId: 1, taskId: "1", completed: 1234567890},
                    headers: {
                        'Authorization': 'wrong API_KEY'
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toEqual(403);
                    expect(body.error).toEqual(true);
                    expect(body.message).toEqual('access forbidden');
                    resolve();
                }
            );
        });
    });


    test('Server returns 404 on not configured route', async () => {
        return new Promise(async (resolve: any, reject: any) => {
            const server = new Server(Config);
    
            
            await server.start(); //.catch(e => console.log(e))
            
    
            reqwest(
                {
                    url: 'http://127.0.0.1:1337/notconfigured',
                    method: 'POST',
                    json: {lorem: 'ipsum'},
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    expect(response.statusCode).toEqual(404);
                    expect(body.error).toEqual(true);
                    expect(body.message).toEqual('route not found');
                    resolve();
                }
            );
        });
    });


    test('Server returns 500 on valid namespace-route when no mysql connection can be established', async () => {
        return new Promise(async (resolve: any, reject: any) => {
            Config.dboConfig.host = '127.0.0.2';
            Config.dboConfig.port = 3000;
            const server = new Server(Config);
    
            await server.start();
            
            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/user/all',
                    method: 'GET',
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    body = JSON.parse(body);
                    expect(response.statusCode).toEqual(500);
                    expect(body.error).toEqual(true);
                    expect(body.message).toEqual('server error');
                    resolve();
                }
            );
        });
    }, 15000); // needed since default is 5000 and because server tries longer to connect to db server than jest waits


    test('Server returns 500 on valid single-route when no mysql connection can be established', async () => {
        return new Promise(async (resolve: any, reject: any) => {
            Config.dboConfig.host = '127.0.0.2';
            Config.dboConfig.port = 3000;
            const server = new Server(Config);
    
            await server.start();
            
            reqwest(
                {
                    url: 'http://127.0.0.1:1337/api/v1/',
                    method: 'GET',
                    headers: {
                        'Authorization': API_KEY
                    }
                },
                async (error, response, body) => {
                    // console.log(body);
                    await server.stop();
                    body = JSON.parse(body);
                    expect(response.statusCode).toEqual(500);
                    expect(body.error).toEqual(true);
                    expect(body.message).toEqual('server error');
                    resolve();
                }
            );
        });
    }, 15000); // needed since default is 5000 and because server tries longer to connect to db server than jest waits

});