import Task from "../src/entity/Task";
import { BaseEntity } from "typeorm";

describe('Entity.Task', () => {

    test('Task inherits BaseEntity', () => {
        const task = new Task();
        
        expect(task).toBeInstanceOf(BaseEntity);
    });

    test('Task.name', () => {
        const user = new Task();
        user.name = 'Niklas';

        expect(user.name).toEqual('Niklas');
    });

    test('Task.desciption', () => {
        const task = new Task();
        task.description = 'Maier';

        expect(task.description).toEqual('Maier');
    });

    test('User.password', () => {
        const task = new Task();
        task.value = 1;

        expect(task.value).toEqual(1);
    });

});