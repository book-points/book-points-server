import IDBOConfig from './interface/IDBOConfig';
import User from './entity/User';
import UserController from './server/route/UserController';
import IConfig from './interface/IConfig';
import { 
    IRoutesConfig, 
    INamespacedRouteAction, 
    IRouteAction, 
    INamespacedServerRoute
} from './interface/IRoutes';
import Task from './entity/Task';
import UserTask from './entity/UserTask';
import TaskController from './server/route/TaskController';
import UserTaskController from './server/route/UserTaskController';
import IndexController from './server/route/IndexController';


const dboConfig : IDBOConfig = {
    name: 'default',
    type: 'mysql',
    host: '127.0.0.1',
    port: 3306,
    username: 'root',
    password: 'root',
    database: 'BookPoints',
    entities: [
        User,
        Task,
        UserTask
    ],
    synchronize: true,
    logging: false
}


const routesConfig: IRoutesConfig = {
    namespaces: Array<INamespacedServerRoute>(
        {
            namespace: 'user',
            callbackClass: UserController,
            routes: Array<INamespacedRouteAction>(
                {
                    method: 'post',
                    path: 'create'
                },
                {
                    method: 'post',
                    path: 'auth'
                },
                {
                    method: 'get',
                    path: 'all'
                }
            )
        },
        {
            namespace: 'task',
            callbackClass: TaskController,
            routes: Array<INamespacedRouteAction>(
                {
                    method: 'post',
                    path: 'create'
                },
                {
                    method: 'get',
                    path: 'all'
                }
            )
        },
        {
            namespace: 'usertask',
            callbackClass: UserTaskController,
            routes: Array<INamespacedRouteAction>(
                {
                    method: 'post',
                    path: 'create'
                },
                {
                    method: 'get',
                    path: 'done'
                },
                {
                    method: 'delete',
                    path: 'remove'
                }
            )
        }
    ),
    singles: Array<IRouteAction>(
        {
            method: 'get',
            path: '',
            callbackClass: IndexController
        }
    )
}



const Config: IConfig = {
    apiVersion: 'v1',
    apiPath: 'api',
    dboConfig,
    routesConfig
}

export default Config;
