import Server from './../server/Server';
import IConfig from './../interface/IConfig';


export default class App {

    // ---------------------------------- Properties ---------------------------------- //
    
    private _server : Server;
    
    public get server() : Server {
        return this._server
    }


    public signals: any;


    // ---------------------------------- Constructor --------------------------------- //
    
    constructor(options : IConfig, globalSignals : any) {

        this.signals = globalSignals;

        this._initConnections(options).then(() => {
            console.log("api up and running on version: " + options.apiVersion);
            globalSignals.ready.dispatch();
        });
    }


    // ---------------------------------- Public -------------------------------------- //
    
    async _initConnections(config : IConfig) {
        this._server = new Server(config);
        await this._server.start();
    }

    // ---------------------------------- Private ------------------------------------- //


}