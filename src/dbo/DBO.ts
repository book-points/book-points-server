import { createConnection, Connection } from "typeorm";
import IDBOConfig from "../interface/iDBOConfig";

export default class DBO
{
    
    protected static _connection : Connection;

    public static get Connection() : Connection {
        return this._connection;
    }


    public static connection(config : IDBOConfig) : Promise<Connection> {
        return new Promise((resolve, reject) => {
            if(this._connection) {
                resolve(this._connection);
            }
            else {
                DBO.connect(config)
                    .then(connection => resolve(connection))
                    .catch(e => reject(e));
            }
        });
    }


    public static async close() {
        await this._connection.close();
        this._connection = undefined;
    }


    protected static connect(config: any) : Promise<Connection> {
        return new Promise((resolve: any, reject: any) => {
            createConnection(config).then((connection : Connection) => {
                this._connection = connection;
                resolve(connection);
            }).catch(e => reject(e));
        })
    }

}