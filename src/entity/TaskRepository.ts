import {EntityRepository, Repository} from 'typeorm';
import Task from './Task';


@EntityRepository(Task)
export class TaskRepository extends Repository<Task> {

    findByName(name: string) {
        return this.findOne({ name });
    }

}