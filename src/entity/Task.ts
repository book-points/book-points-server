import { Entity, Column, OneToMany, ManyToMany } from 'typeorm';
import BaseEntity from './Base';
import UserTask from './UserTask';
import User from './User';

@Entity()
export default class Task extends BaseEntity {

    @Column({type: 'varchar', length: 100, unique: true})
    name: string;

    @Column({type: 'varchar', length: 400})
    description: string;

    @Column({type: 'int', length: 100})
    value: number;

    @OneToMany(type => UserTask, task => task.user)
    users: UserTask[];

}