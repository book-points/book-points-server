import { Entity, Column, OneToMany, JoinTable, ManyToMany } from 'typeorm';
import BaseEntity from './Base';
import Task from './Task';
import UserTask from './UserTask';
 
@Entity()
export default class User extends BaseEntity {
 
    private _firstName : string;
    
    @Column({ type: 'varchar', length: 40, unique: true })
    public get firstName() : string {
        return this._firstName;
    }

    public set firstName(v : string) {
        this._firstName = v;
    }
    
 
    @Column({ type: 'varchar', length: 40 })
    lastName: string;

    
    @Column({ type: 'varchar', length: 200 })
    password: string;

 
    @OneToMany(type => UserTask, task => task.user, {
        //eager true means the relation-entity is loaded into this entity
        eager: true
    })
    @JoinTable()
    tasks: UserTask[];
}