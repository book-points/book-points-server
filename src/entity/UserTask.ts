import { Entity, Column, JoinTable, ManyToOne, ManyToMany } from 'typeorm';
import BaseEntity from './Base';
import Task from './Task';
import User from './User';

@Entity()
export default class UserTask extends BaseEntity {

    @ManyToOne(type => User, user => user.tasks)
    user: User

    @ManyToOne(type => Task, task => task.users, {
        //eager true means the relation-entity is loaded into this entity
        eager: true
    })
    task: Task

    @Column({ type: 'datetime'})
    completed: Date

}