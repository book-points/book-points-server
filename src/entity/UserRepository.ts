import {EntityRepository, Repository} from 'typeorm';
import User from './User';
import UserTask from './UserTask';


@EntityRepository(User)
export class UserRepository extends Repository<User> {

    async findByFirstName(firstName: string) {
        return this.findOne({ firstName });
    }


    async getCompletedTasksForCurrentMonth(userId : number) {
        const user = await this.findOneById(userId),
            now = new Date(Date.now());

        const result = user.tasks.filter((usertask : UserTask) => {
            if(now.getMonth() === new Date(usertask.completed).getMonth()) {
                return usertask;
            }
        });

        return result;
    }

}