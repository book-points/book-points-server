import { PrimaryGeneratedColumn, BaseEntity, Column, BeforeUpdate, BeforeInsert } from 'typeorm';

export default class Base extends BaseEntity
{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: 'datetime'})
    created: Date;

    @Column({type: 'datetime'})
    modified: Date;

    @BeforeUpdate()
    updateModified() {
        this.modified = new Date();
    }

    @BeforeInsert()
    updateCreated() {
        this.created = new Date();
    }
}