import IDBOConfig from './IDBOConfig';

export default interface IConfig {
    apiVersion : string,
    apiPath : string,
    dboConfig : IDBOConfig
    routesConfig : any
}