
export interface IRoutesConfig {
    namespaces: Array<INamespacedServerRoute>,
    singles: Array<IRouteAction>
}

export interface INamespacedRouteAction {
    method: string,
    path: string
}

export interface IRouteAction extends INamespacedRouteAction {
    method: string,
    path: string,
    callbackClass: any
}

export interface INamespacedServerRoute {
    namespace: string,
    callbackClass: any,
    routes: Array<INamespacedRouteAction>
}