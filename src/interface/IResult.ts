export default interface IResult {
    error: boolean,
    message: string,
    result: Object
}