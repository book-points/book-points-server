import * as express from 'express';
import RestController from "./RestController";
import { Connection, getCustomRepository } from 'typeorm';

export default class IndexController extends RestController {
    // ---------------------------------- Properties ---------------------------------- //




    // ---------------------------------- Constructor --------------------------------- //

    constructor(request: express.Request, response: express.Response) {
        super(request, response);

        this.answer({error: false, message: 'index-route', result: {}}, 200);
    }


    // ---------------------------------- Actions ------------------------------------- //

}