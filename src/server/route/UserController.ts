import * as express from 'express';
import * as bcrypt from 'bcrypt';
import User from './../../entity/User';
import DBO from './../../dbo/DBO';
import { Connection, Repository, getCustomRepository } from 'typeorm';
import RestController from './RestController';
import { UserRepository } from '../../entity/UserRepository';
import UserTask from '../../entity/UserTask';



export default class UserController extends RestController {

    // ---------------------------------- Properties ---------------------------------- //

    private _repository : UserRepository;


    // ---------------------------------- Constructor --------------------------------- //

    constructor(request: express.Request, response: express.Response) {
        super(request, response);

        this._repository = getCustomRepository(UserRepository);
    }


    // ---------------------------------- Actions ------------------------------------- //

    /**
     * @param dboConnection {Connection}
     */
    async create(dboConnection : Connection) {
        if(this.assertParameters(['firstName', 'lastName', 'password'])) {
            const doubled = await this._repository.findByFirstName(this.request.body.firstName);

            if(doubled === undefined) {
                const user = new User(),
                hash = await bcrypt.hash(this.request.body.password, 10);
    
                user.firstName = this.request.body.firstName;
                user.lastName = this.request.body.lastName;
                user.password = hash;
    
                const saved = await dboConnection.manager.save(user);
    
                this.answer({error: false, message: 'user successfully created', result: { id: user.id }}, 200);
            }
            else if(doubled !== undefined) {
                this.answer({error: true, message: 'firstName already taken', result: {}}, 400);
            }
        }
        else {
            this.answer({error: true, message: 'invalid parameters send', result: {}}, 400);
        }
    }


    /**
     * 
     * @param dboConnection {Connection}
     */
    async auth(dboConnection : Connection) {
        if(this.assertParameters(['firstName', 'password'])) {
            const user = await this._repository.findByFirstName(this.request.body.firstName),
            valid = await bcrypt.compareSync(this.request.body.password, user.password);

            if(valid === true) {
                this.answer({error: false, message: 'password valid', result: { id: user.id }}, 200);
            }
            else {
                this.answer({error: true, message: 'password invalid', result: {}}, 400);
            }
        }
        else {
            this.answer({error: true, message: 'invalid parameters send', result: {}}, 400);
        }
    }


    async all(dboConnection : Connection) {
        const users = await this._repository.find();

        let result = Array();

        for(const user of users) {
            const completedTasks = await this._repository.getCompletedTasksForCurrentMonth(user.id);

            let userMonthPoints = 0;
            completedTasks.forEach((usertask: UserTask) => {
                userMonthPoints += usertask.task.value;
            });

            result.push({
                firstName: user.firstName,
                totalPoints: userMonthPoints
            });
        }

        this.answer({error: false, message: '', result: result}, 200);
    }
}