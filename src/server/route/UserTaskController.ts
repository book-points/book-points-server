import * as express from 'express';
import RestController from './RestController';
import { Connection, getCustomRepository, getRepository, Repository } from 'typeorm';
import UserTask from '../../entity/UserTask';
import { UserRepository } from '../../entity/UserRepository';
import { TaskRepository } from '../../entity/TaskRepository';


export default class UserTaskController extends RestController {
    // ---------------------------------- Properties ---------------------------------- //

    private _userRepository : UserRepository;

    private _taskRepository : TaskRepository;

    private _repository : Repository<UserTask>;


    // ---------------------------------- Constructor --------------------------------- //

    constructor(request: express.Request, response: express.Response) {
        super(request, response);

        this._userRepository = getCustomRepository(UserRepository);
        this._taskRepository = getCustomRepository(TaskRepository);
        this._repository = getRepository(UserTask);
    }


    // ---------------------------------- Actions ------------------------------------- //

    async create(dboConnection: Connection) {
        if(this.assertParameters(['userId', 'taskId', 'completed'])) {
            const ut = new UserTask(),
                user = await this._userRepository.findOneById(this.request.body.userId),
                task = await this._taskRepository.findOneById(this.request.body.taskId),
                date = new Date(this.request.body.completed);
            
            if(user && task && date) {
                ut.user = user;
                ut.task = task;
                ut.completed = new Date(parseInt(this.request.body.completed));

                const saved = await dboConnection.manager.save(ut);
    
                this.answer({error: false, message: 'usertask successfully created', result: { userTaskId: ut.id }}, 200);
            }
            else {
                this.answer({error: true, message: 'unmatched parameters send', result: {}}, 400);
            }
        }
        else {
            this.answer({error: true, message: 'invalid parameters send', result: {}}, 400);
        }
    }


    async done(dboConnection: Connection) {
        if(this.assertParameters(['userId'])) {
            const completedTasks = await this._userRepository.getCompletedTasksForCurrentMonth(parseInt(this.request.query['userId']));
            let result = Array(),
                totalPoints = 0;

            completedTasks.forEach((usertask : UserTask) => {
                result.push({
                    id: usertask.task.id,
                    name: usertask.task.name,
                    description: usertask.task.description,
                    value: usertask.task.value,
                    completed: new Date(usertask.completed).getTime()
                });

                totalPoints += usertask.task.value;
            });

            this.answer({error: false, message: '', result: {totalPoints: totalPoints, tasks: result}}, 200);
        }
        else {
            this.answer({error: true, message: 'invalid parameters send', result: {}}, 400);
        }
    }


    async remove(dboConnection: Connection) {
        //TODO enable DELETE http method again in server cors config
    }
}