import * as express from 'express';
import { stat } from 'fs';
import IResult from '../../interface/IResult';

export default class RestController {

    // ---------------------------------- Properties ---------------------------------- //
    
    private _request : express.Request;

    public get request() : express.Request {
        return this._request;
    }

    public set request(v : express.Request) {
        this._request = v;
    }
    

    
    private _response : express.Response;

    public get response() : express.Response {
        return this._response;
    }

    public set response(v : express.Response) {
        this._response = v;
    }
    

    // ---------------------------------- Constructor --------------------------------- //
    
    constructor(request: express.Request, response: express.Response) {
        this._request = request;
        this._response = response;
    }
    
    // ---------------------------------- Private ------------------------------------- //

    // ---------------------------------- Public -------------------------------------- //

    answer(result : IResult, status : number) {
        this.response.status(status);
        this.response.json(result);
    }


    assertParameters(parameters : any) {
        let assertion = true;
        for(const param of parameters) {
            if((this._request.body.hasOwnProperty(param) === false || this._request.body[param] === '' )
                && (this._request.query.hasOwnProperty(param) === false || this._request.query[param] === '') && assertion ) {
                    assertion = false;
                    break;
            }
        }
        return assertion;
    }
}