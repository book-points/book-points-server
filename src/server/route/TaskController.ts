import * as express from 'express';
import RestController from "./RestController";
import { Connection, getCustomRepository } from 'typeorm';
import Task from '../../entity/Task';
import { TaskRepository } from '../../entity/TaskRepository';

export default class TaskController extends RestController {
    // ---------------------------------- Properties ---------------------------------- //

    private _repository : TaskRepository;


    // ---------------------------------- Constructor --------------------------------- //

    constructor(request: express.Request, response: express.Response) {
        super(request, response);

        this._repository = getCustomRepository(TaskRepository);
    }


    // ---------------------------------- Actions ------------------------------------- //

    async create(dboConnection: Connection) {
        if(this.assertParameters(['name', 'description', 'value'])) {
            const doubled = await this._repository.findByName(this.request.body.name);

            if(doubled === undefined) {
                const task = new Task();

                task.name = this.request.body.name;
                task.description = this.request.body.description;
                task.value = parseInt(this.request.body.value);

                const saved = await dboConnection.manager.save(task);

                this.answer({error: false, message: 'task successfully created', result: { taskId: task.id }}, 200);
            }
            else if(doubled !== undefined) {
                this.answer({error: true, message: 'task.name already taken', result: {}}, 400);
            }
        }
        else {
            this.answer({error: true, message: 'invalid parameters send', result: {}}, 400);
        }
    }


    async all(dboConnection: Connection) {
        const all = await this._repository.find();

        const prepared = all.map(task => {
            return {
                id: task.id,
                name: task.name,
                description: task.description,
                value: task.value
            }
        });

        this.answer({error: false, message: '', result: prepared}, 200);
    }
}