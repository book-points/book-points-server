import * as path from 'path';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import DBO from '../dbo/DBO';
import IConfig from '../interface/IConfig';
import IDBOConfig from '../interface/IDBOConfig';
import { getConnection } from 'typeorm';
import { IRouteAction, INamespacedServerRoute, IRoutesConfig } from '../interface/IRoutes';

export default class Server {

    // ---------------------------------- Properties ---------------------------------- //

    private _http : any;
    
    get http() : any {
        return this._http;
    }

    
    private _node : any;

    public get node() : any {
        return this._node;
    }

    public set node(v : any) {
        this._node = v;
    }


    private _apiPath : string;

    private _apiVersion : string;


    // ---------------------------------- Constructor --------------------------------- //

    constructor(options : IConfig) {
        this._http = express();
        this._apiPath = options.apiPath;
        this._apiVersion = options.apiVersion;

        this._initRoutes(options.routesConfig, options);
    }


    // ---------------------------------- Public -------------------------------------- //

    start(port : number = 0) {
        return new Promise((resolve: any, reject: any) => {
            this._node = this._http.listen(port != 0 ? port : 1337, () => {
                console.log('App listening on port: ' + (port != 0 ? port : 1337));
                resolve(true);
            });
        });
    }


    async stop() {
        await this._node.close();
    }


    getFullApiUrl(endpoint : string) {
        return path.sep + this._apiPath + path.sep + this._apiVersion + path.sep + endpoint;
    }


    addRoute(route : IRouteAction, dboConfig : IDBOConfig) {
        this._http[route.method](this.getFullApiUrl(route.path), async (req: express.Request, res: express.Response) => {
            let valid = true;
            const dboConnection = await DBO.connection(dboConfig)
                .catch(e => {
                    valid = false;
                    this._serverError(res);
                });

            if(valid) {
                new route.callbackClass(req, res, dboConnection);
            }
        });
    }


    addRouteController(router: any, action: IRouteAction, route: INamespacedServerRoute, dboConfig : IDBOConfig) {
        router[action.method](path.sep + action.path, async (request: express.Request, response: express.Response) => {
            let valid = true;
            const dboConnection = await DBO.connection(dboConfig)
                    .catch(e => {
                        valid = false;
                        this._serverError(response);
                    });

            if(valid) {
                const controller = new route.callbackClass(request, response);
                
                controller[action.path](dboConnection)
                    .catch((e: any) => {
                        this._serverError(response);
                    });
            }

        });

        return router;
    }
    
    
    // ---------------------------------- Private ------------------------------------- //

    private _initRoutes(routesConfig: IRoutesConfig, config : IConfig) {
        //for better request parameter parsing
        this._http.use(bodyParser.json()); // support json encoded bodies
        this._http.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

        //enable cors
        this._http.use(cors({
            // origin: '*',
            methods: 'GET,POST,OPTIONS'
        }));

        // hook being fired before any call to the API
        this._http.all('/*', function (req: express.Request, res: express.Response, next: any) {
            if(req.method !== 'OPTIONS') {
                const authHeader = req.get('Authorization');
                if(authHeader !== 'bljS+l/.9x|aN!LHrP/7bd0zOcLwBHBZet]PDH%udkvfmcMEkv&1l4uY{;FN[0K&') {
                    res.status(403);
                    res.json({error: true, message: 'access forbidden'});
                }
                else {
                    next() // pass control to the next handler
                }
            }
        });
        
        routesConfig.namespaces.forEach(route => {
            let router = express.Router();

            route.routes.forEach((action : IRouteAction) => {

                router = this.addRouteController(router, action, route, config.dboConfig);

            });

            this._http.use(this.getFullApiUrl(route.namespace), router);
        });

        routesConfig.singles.forEach(route => {
            this.addRoute(route, config.dboConfig);
        });

        // not found route hook
        this._http.use((req: express.Request, res: express.Response, next: any) => {
            res.status(404);
            res.json({error: true, message: 'route not found'});
        });
    }


    private _serverError(res: express.Response) {
        res.status(500);
        res.json({error: true, message: 'server error', result: {}});
    }

}