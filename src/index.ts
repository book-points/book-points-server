import { Signal } from 'signals';
import configDev from './index.config';
import configProd from './index.config.prod';
import App from './app/App';


//decide which config to use depending on NODE_ENV
let config;
if(process.env.NODE_ENV === 'production') {
    config = configProd;
}
else {
    config = configDev;
}


//initialize signals
const globalSignals = {
    ready: new Signal()
};


//exports
export default {
    App: App,
    app: new App(config, globalSignals),
    globalSignals
};