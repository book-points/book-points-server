# Uberspace
## node server cli stuff
restart book-points-server service (after release/upload):

    supervisorctl restart bookpointsserver

reread service file (niq/etc/services.d/bookpointsserver.ini) to check if changes were detected

    supervisorctl reread

update service, read service file into ram:

    supervisorctl update

stop service to maybe run it manually to debug:

    supervisorctl stop bookpointsserver


# content of service-file on node server

    [program:bookpointsserver]
    directory=/home/niq/book-points-server
    command=npm run prod
    autostart=true
    autorestart=true
