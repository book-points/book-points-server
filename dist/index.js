"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const signals_1 = require("signals");
const index_config_1 = require("./index.config");
const index_config_prod_1 = require("./index.config.prod");
const App_1 = require("./app/App");
let config;
if (process.env.NODE_ENV === 'production') {
    config = index_config_prod_1.default;
}
else {
    config = index_config_1.default;
}
const globalSignals = {
    ready: new signals_1.Signal()
};
exports.default = {
    App: App_1.default,
    app: new App_1.default(config, globalSignals),
    globalSignals
};
//# sourceMappingURL=index.js.map