"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Server_1 = require("./../server/Server");
class App {
    get server() {
        return this._server;
    }
    constructor(options, globalSignals) {
        this.signals = globalSignals;
        this._initConnections(options).then(() => {
            console.log("api up and running on version: " + options.apiVersion);
            globalSignals.ready.dispatch();
        });
    }
    _initConnections(config) {
        return __awaiter(this, void 0, void 0, function* () {
            this._server = new Server_1.default(config);
            yield this._server.start();
        });
    }
}
exports.default = App;
//# sourceMappingURL=App.js.map