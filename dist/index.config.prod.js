"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("./entity/User");
const UserController_1 = require("./server/route/UserController");
const Task_1 = require("./entity/Task");
const UserTask_1 = require("./entity/UserTask");
const TaskController_1 = require("./server/route/TaskController");
const UserTaskController_1 = require("./server/route/UserTaskController");
const IndexController_1 = require("./server/route/IndexController");
const dboConfig = {
    name: 'default',
    type: 'mysql',
    host: '127.0.0.1',
    port: 3306,
    username: 'niq',
    password: 'sjIT-4A6W1p3eP.PZU58',
    database: 'niq',
    entities: [
        User_1.default,
        Task_1.default,
        UserTask_1.default
    ],
    synchronize: true,
    logging: false
};
const routesConfig = {
    namespaces: Array({
        namespace: 'user',
        callbackClass: UserController_1.default,
        routes: Array({
            method: 'post',
            path: 'create'
        }, {
            method: 'post',
            path: 'auth'
        }, {
            method: 'get',
            path: 'all'
        })
    }, {
        namespace: 'task',
        callbackClass: TaskController_1.default,
        routes: Array({
            method: 'post',
            path: 'create'
        }, {
            method: 'get',
            path: 'all'
        })
    }, {
        namespace: 'usertask',
        callbackClass: UserTaskController_1.default,
        routes: Array({
            method: 'post',
            path: 'create'
        }, {
            method: 'get',
            path: 'done'
        }, {
            method: 'delete',
            path: 'remove'
        })
    }),
    singles: Array({
        method: 'get',
        path: '',
        callbackClass: IndexController_1.default
    })
};
const Config = {
    apiVersion: 'v1',
    apiPath: 'api',
    dboConfig,
    routesConfig
};
exports.default = Config;
//# sourceMappingURL=index.config.prod.js.map