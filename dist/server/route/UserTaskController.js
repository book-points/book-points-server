"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const RestController_1 = require("./RestController");
const typeorm_1 = require("typeorm");
const UserTask_1 = require("../../entity/UserTask");
const UserRepository_1 = require("../../entity/UserRepository");
const TaskRepository_1 = require("../../entity/TaskRepository");
class UserTaskController extends RestController_1.default {
    constructor(request, response) {
        super(request, response);
        this._userRepository = typeorm_1.getCustomRepository(UserRepository_1.UserRepository);
        this._taskRepository = typeorm_1.getCustomRepository(TaskRepository_1.TaskRepository);
        this._repository = typeorm_1.getRepository(UserTask_1.default);
    }
    create(dboConnection) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.assertParameters(['userId', 'taskId', 'completed'])) {
                const ut = new UserTask_1.default(), user = yield this._userRepository.findOneById(this.request.body.userId), task = yield this._taskRepository.findOneById(this.request.body.taskId), date = new Date(this.request.body.completed);
                if (user && task && date) {
                    ut.user = user;
                    ut.task = task;
                    ut.completed = new Date(parseInt(this.request.body.completed));
                    const saved = yield dboConnection.manager.save(ut);
                    this.answer({ error: false, message: 'usertask successfully created', result: { userTaskId: ut.id } }, 200);
                }
                else {
                    this.answer({ error: true, message: 'unmatched parameters send', result: {} }, 400);
                }
            }
            else {
                this.answer({ error: true, message: 'invalid parameters send', result: {} }, 400);
            }
        });
    }
    done(dboConnection) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.assertParameters(['userId'])) {
                const completedTasks = yield this._userRepository.getCompletedTasksForCurrentMonth(parseInt(this.request.query['userId']));
                let result = Array(), totalPoints = 0;
                completedTasks.forEach((usertask) => {
                    result.push({
                        id: usertask.task.id,
                        name: usertask.task.name,
                        description: usertask.task.description,
                        value: usertask.task.value,
                        completed: new Date(usertask.completed).getTime()
                    });
                    totalPoints += usertask.task.value;
                });
                this.answer({ error: false, message: '', result: { totalPoints: totalPoints, tasks: result } }, 200);
            }
            else {
                this.answer({ error: true, message: 'invalid parameters send', result: {} }, 400);
            }
        });
    }
    remove(dboConnection) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
}
exports.default = UserTaskController;
//# sourceMappingURL=UserTaskController.js.map