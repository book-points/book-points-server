"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const RestController_1 = require("./RestController");
class IndexController extends RestController_1.default {
    constructor(request, response) {
        super(request, response);
        this.answer({ error: false, message: 'index-route', result: {} }, 200);
    }
}
exports.default = IndexController;
//# sourceMappingURL=IndexController.js.map