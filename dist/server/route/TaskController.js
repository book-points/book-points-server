"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const RestController_1 = require("./RestController");
const typeorm_1 = require("typeorm");
const Task_1 = require("../../entity/Task");
const TaskRepository_1 = require("../../entity/TaskRepository");
class TaskController extends RestController_1.default {
    constructor(request, response) {
        super(request, response);
        this._repository = typeorm_1.getCustomRepository(TaskRepository_1.TaskRepository);
    }
    create(dboConnection) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.assertParameters(['name', 'description', 'value'])) {
                const doubled = yield this._repository.findByName(this.request.body.name);
                if (doubled === undefined) {
                    const task = new Task_1.default();
                    task.name = this.request.body.name;
                    task.description = this.request.body.description;
                    task.value = parseInt(this.request.body.value);
                    const saved = yield dboConnection.manager.save(task);
                    this.answer({ error: false, message: 'task successfully created', result: { taskId: task.id } }, 200);
                }
                else if (doubled !== undefined) {
                    this.answer({ error: true, message: 'task.name already taken', result: {} }, 400);
                }
            }
            else {
                this.answer({ error: true, message: 'invalid parameters send', result: {} }, 400);
            }
        });
    }
    all(dboConnection) {
        return __awaiter(this, void 0, void 0, function* () {
            const all = yield this._repository.find();
            const prepared = all.map(task => {
                return {
                    id: task.id,
                    name: task.name,
                    description: task.description,
                    value: task.value
                };
            });
            this.answer({ error: false, message: '', result: prepared }, 200);
        });
    }
}
exports.default = TaskController;
//# sourceMappingURL=TaskController.js.map