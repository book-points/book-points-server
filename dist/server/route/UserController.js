"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcrypt");
const User_1 = require("./../../entity/User");
const typeorm_1 = require("typeorm");
const RestController_1 = require("./RestController");
const UserRepository_1 = require("../../entity/UserRepository");
class UserController extends RestController_1.default {
    constructor(request, response) {
        super(request, response);
        this._repository = typeorm_1.getCustomRepository(UserRepository_1.UserRepository);
    }
    create(dboConnection) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.assertParameters(['firstName', 'lastName', 'password'])) {
                const doubled = yield this._repository.findByFirstName(this.request.body.firstName);
                if (doubled === undefined) {
                    const user = new User_1.default(), hash = yield bcrypt.hash(this.request.body.password, 10);
                    user.firstName = this.request.body.firstName;
                    user.lastName = this.request.body.lastName;
                    user.password = hash;
                    const saved = yield dboConnection.manager.save(user);
                    this.answer({ error: false, message: 'user successfully created', result: { id: user.id } }, 200);
                }
                else if (doubled !== undefined) {
                    this.answer({ error: true, message: 'firstName already taken', result: {} }, 400);
                }
            }
            else {
                this.answer({ error: true, message: 'invalid parameters send', result: {} }, 400);
            }
        });
    }
    auth(dboConnection) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.assertParameters(['firstName', 'password'])) {
                const user = yield this._repository.findByFirstName(this.request.body.firstName), valid = yield bcrypt.compareSync(this.request.body.password, user.password);
                if (valid === true) {
                    this.answer({ error: false, message: 'password valid', result: { id: user.id } }, 200);
                }
                else {
                    this.answer({ error: true, message: 'password invalid', result: {} }, 400);
                }
            }
            else {
                this.answer({ error: true, message: 'invalid parameters send', result: {} }, 400);
            }
        });
    }
    all(dboConnection) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this._repository.find();
            let result = Array();
            for (const user of users) {
                const completedTasks = yield this._repository.getCompletedTasksForCurrentMonth(user.id);
                let userMonthPoints = 0;
                completedTasks.forEach((usertask) => {
                    userMonthPoints += usertask.task.value;
                });
                result.push({
                    firstName: user.firstName,
                    totalPoints: userMonthPoints
                });
            }
            this.answer({ error: false, message: '', result: result }, 200);
        });
    }
}
exports.default = UserController;
//# sourceMappingURL=UserController.js.map