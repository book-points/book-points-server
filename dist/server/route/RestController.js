"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RestController {
    get request() {
        return this._request;
    }
    set request(v) {
        this._request = v;
    }
    get response() {
        return this._response;
    }
    set response(v) {
        this._response = v;
    }
    constructor(request, response) {
        this._request = request;
        this._response = response;
    }
    answer(result, status) {
        this.response.status(status);
        this.response.json(result);
    }
    assertParameters(parameters) {
        let assertion = true;
        for (const param of parameters) {
            if ((this._request.body.hasOwnProperty(param) === false || this._request.body[param] === '')
                && (this._request.query.hasOwnProperty(param) === false || this._request.query[param] === '') && assertion) {
                assertion = false;
                break;
            }
        }
        return assertion;
    }
}
exports.default = RestController;
//# sourceMappingURL=RestController.js.map