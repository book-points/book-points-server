"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const DBO_1 = require("../dbo/DBO");
class Server {
    get http() {
        return this._http;
    }
    get node() {
        return this._node;
    }
    set node(v) {
        this._node = v;
    }
    constructor(options) {
        this._http = express();
        this._apiPath = options.apiPath;
        this._apiVersion = options.apiVersion;
        this._initRoutes(options.routesConfig, options);
    }
    start(port = 0) {
        return new Promise((resolve, reject) => {
            this._node = this._http.listen(port != 0 ? port : 1337, () => {
                console.log('App listening on port: ' + (port != 0 ? port : 1337));
                resolve(true);
            });
        });
    }
    stop() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._node.close();
        });
    }
    getFullApiUrl(endpoint) {
        return path.sep + this._apiPath + path.sep + this._apiVersion + path.sep + endpoint;
    }
    addRoute(route, dboConfig) {
        this._http[route.method](this.getFullApiUrl(route.path), (req, res) => __awaiter(this, void 0, void 0, function* () {
            let valid = true;
            const dboConnection = yield DBO_1.default.connection(dboConfig)
                .catch(e => {
                valid = false;
                this._serverError(res);
            });
            if (valid) {
                new route.callbackClass(req, res, dboConnection);
            }
        }));
    }
    addRouteController(router, action, route, dboConfig) {
        router[action.method](path.sep + action.path, (request, response) => __awaiter(this, void 0, void 0, function* () {
            let valid = true;
            const dboConnection = yield DBO_1.default.connection(dboConfig)
                .catch(e => {
                valid = false;
                this._serverError(response);
            });
            if (valid) {
                const controller = new route.callbackClass(request, response);
                controller[action.path](dboConnection)
                    .catch((e) => {
                    this._serverError(response);
                });
            }
        }));
        return router;
    }
    _initRoutes(routesConfig, config) {
        this._http.use(bodyParser.json());
        this._http.use(bodyParser.urlencoded({ extended: true }));
        this._http.use(cors({
            methods: 'GET,POST,OPTIONS'
        }));
        this._http.all('/*', function (req, res, next) {
            if (req.method !== 'OPTIONS') {
                const authHeader = req.get('Authorization');
                if (authHeader !== 'bljS+l/.9x|aN!LHrP/7bd0zOcLwBHBZet]PDH%udkvfmcMEkv&1l4uY{;FN[0K&') {
                    res.status(403);
                    res.json({ error: true, message: 'access forbidden' });
                }
                else {
                    next();
                }
            }
        });
        routesConfig.namespaces.forEach(route => {
            let router = express.Router();
            route.routes.forEach((action) => {
                router = this.addRouteController(router, action, route, config.dboConfig);
            });
            this._http.use(this.getFullApiUrl(route.namespace), router);
        });
        routesConfig.singles.forEach(route => {
            this.addRoute(route, config.dboConfig);
        });
        this._http.use((req, res, next) => {
            res.status(404);
            res.json({ error: true, message: 'route not found' });
        });
    }
    _serverError(res) {
        res.status(500);
        res.json({ error: true, message: 'server error', result: {} });
    }
}
exports.default = Server;
//# sourceMappingURL=Server.js.map