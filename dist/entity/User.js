"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Base_1 = require("./Base");
const UserTask_1 = require("./UserTask");
let User = class User extends Base_1.default {
    get firstName() {
        return this._firstName;
    }
    set firstName(v) {
        this._firstName = v;
    }
};
__decorate([
    typeorm_1.Column({ type: 'varchar', length: 40, unique: true }),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [String])
], User.prototype, "firstName", null);
__decorate([
    typeorm_1.Column({ type: 'varchar', length: 40 }),
    __metadata("design:type", String)
], User.prototype, "lastName", void 0);
__decorate([
    typeorm_1.Column({ type: 'varchar', length: 200 }),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    typeorm_1.OneToMany(type => UserTask_1.default, task => task.user, {
        eager: true
    }),
    typeorm_1.JoinTable(),
    __metadata("design:type", Array)
], User.prototype, "tasks", void 0);
User = __decorate([
    typeorm_1.Entity()
], User);
exports.default = User;
//# sourceMappingURL=User.js.map