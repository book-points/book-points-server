"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class DBO {
    static get Connection() {
        return this._connection;
    }
    static connection(config) {
        return new Promise((resolve, reject) => {
            if (this._connection) {
                resolve(this._connection);
            }
            else {
                DBO.connect(config)
                    .then(connection => resolve(connection))
                    .catch(e => reject(e));
            }
        });
    }
    static close() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._connection.close();
            this._connection = undefined;
        });
    }
    static connect(config) {
        return new Promise((resolve, reject) => {
            typeorm_1.createConnection(config).then((connection) => {
                this._connection = connection;
                resolve(connection);
            }).catch(e => reject(e));
        });
    }
}
exports.default = DBO;
//# sourceMappingURL=DBO.js.map